##  二零一九年十二月-更新记录
- 2019年12月4日  
    - 普通上传、图片处理（裁剪/旋转)
    - 分片上传模式
    - 元数据注解处理、封装附件面板配置
    - 附件转换器工厂、附件转换File对象、附件机制实体、附件机制系统配置
    - 上传附件实现接口、附件上传校验
- 2019年12月5日
    - 完善简单上传模式
- 2019年12月9日
    - 设计代理工场与各大运营商实现接口
    - 上传临时附件本地存储,阿里云OSS实现
    - 附件存储扩展provider/config,数据写入到文件系统
    - 附件模块目录管理
    - 文件本地存储流代理实现、本地文件存储加密处理
- 2019年12月10日
    - 基础字段-新增排序号字段、新增机制类数据
    - 附件基础信息抽离、附件底层文件信息抽离
    - 附件信息表、底层信息表设计
    - 从已有文件中查找相同文件\ @TODO ``如果是媒体，获取媒体宽高帧率待处理``!!!
    - 读取文件流 ``包含扩展名（可以没有）的文件路径，用来确定文件的唯一位置，可直接用作OSS的对象名``
- 2019年12月11日
    - 执行上传操作逻辑实现
    - 分片摘要信息存储
    - 分片上传构建与基础方法实现
    - 完整附件，并返回临时附件文件ID、判断是否可以支持追加写入待实现
- 2019年12月12日
    - 追加写入文件，支持的扩展点重写此方法
    - 判断是否最后一个分片判断处理，整理分片信息
    - 追加写入文件流，不存在则新建文件本地存储实现
- 2019年12月24日
    - 添加附件管理机制依赖
    - 配置服务持久化处理
    - 上传临时附件，并保存临时附件记录
- 2019年12月25日
    - 添加组织权限管理模块
    - 字段名称-支持多语言
    - 添加组织模块基础依赖配置
- 2019年12月28日
    - 树形Entity操作，通过依赖注入调用
    - 抽象组织架构服务层，提供各种组织架构公共方法
    - 组织架构基础数据模型
    - 组织类别（可用于：机构/部门、群组） SysOrgCategory;组织架构元素（包含：机构、部门、岗位、群组、人员）
- 2019年12月30日
    - 组织架构-批量置为无效
    - 批量修改上级
    - 更新机构和第一层级组织的层级关系
    - 群组处理、``@DiscriminatorColumn``映射共享表处理